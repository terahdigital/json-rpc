<?php  declare(strict_types=1);

namespace Terah\JsonRpc;

use JsonSerializable;

class RpcFieldErrorCollection implements JsonSerializable
{
    /** @var array|RpcFieldError[] */
    protected array $fieldErrors = [];

    /**
     * @return array|RpcFieldError[]
     */
    public function getFieldErrors(): array
    {
        return $this->fieldErrors;
    }

    /**
     * @param array|RpcFieldError[] $fieldErrors
     * @return RpcFieldErrorCollection
     */
    public function setFieldErrors(array $fieldErrors) : RpcFieldErrorCollection
    {
        $this->fieldErrors  = [];
        foreach ( $fieldErrors as $error )
        {
            $this->appendFieldError($error);
        }

        return $this;
    }


    public function appendFieldError(RpcFieldError $fieldError) : RpcFieldErrorCollection
    {
        $this->fieldErrors[] = $fieldError;

        return $this;
    }


    public function jsonSerialize()
    {
        return $this->getFieldErrors();
    }
}
