<?php declare(strict_types=1);


namespace Terah\JsonRpc;

use JsonSerializable;
use stdClass;
use Terah\Assert\Assert;
use ArrayObject;

class RpcBatchRequest extends ArrayObject implements JsonSerializable
{
    /**
     * RpcBatchRequest constructor.
     *
     * @param array|stdClass|string $data
     */
    public function __construct($data=[])
    {
        Assert::that($data)
            ->code(RpcError::ERROR_INVALID_JSON)
            ->notEmpty('Parse error: Invalid JSON was received by the server.');

        $this->setFlags(ArrayObject::STD_PROP_LIST|ArrayObject::ARRAY_AS_PROPS);
        $data                   = is_string($data) ? json_decode($data) : $data;
        $data                   = is_array($data) ? $data : [$data];

        foreach ( $data as $request )
        {
            $this->append($request instanceof RpcRequest ? $request : new RpcRequest($request));
        }
    }

    /**
     * @param mixed $value
     * @return RpcBatchRequest
     */
    public function append($value) : RpcBatchRequest
    {
        Assert::that($value)->isInstanceOf(RpcRequest::class);
        parent::append($value);

        return $this;
    }

    /**
     * @return RpcRequest[]|RpcRequest
     */
    public function jsonSerialize()
    {
        if ( $this->count() === 1 )
        {
            return $this->offsetGet(0);
        }

        return $this->getArrayCopy();
    }
}
