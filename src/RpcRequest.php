<?php declare(strict_types=1);

namespace Terah\JsonRpc;

use JsonSerializable;
use Terah\Assert\Assert;
use stdClass;

class RpcRequest implements JsonSerializable
{
    protected string $jsonrpc   = '2.0';

    protected string $method    = '';

    /** @var mixed  */
    protected $params           = null;

    /** @var mixed */
    protected $id               = null;

    /**
     * RpcRequest constructor.
     *
     * @param stdClass|null $data
     */
    public function __construct(stdClass $data=null)
    {
        if ( ! $data )
        {
            return ;
        }
        Assert::that($data)
            ->code(RpcError::ERROR_INVALID_JSON)
            ->isObject('Parse error: Invalid JSON was received by the server.');

        Assert::that($data)
            ->code(RpcError::ERROR_INVALID_REQUEST)
            ->propertiesExist(['jsonrpc', 'method', 'params', 'id'], 'Invalid Request: The JSON sent is not a valid Request object.');

        $this
            ->setJsonrpc($data->jsonrpc)
            ->setId($data->id)
            ->setMethod($data->method)
            ->setParams($data->params);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        $this->requestAssert($this->id)->notEmpty('The jsonrpc ID property not be empty');

        return $this->id;
    }

    /**
     * @param string|int $id
     * @return RpcRequest
     */
    public function setId($id) : RpcRequest
    {
        $this->requestAssert($id)->notEmpty('The jsonrpc ID property not be empty');

        $this->id = $id;

        return $this;
    }


    public function setRandomId(string $prefix='') : RpcRequest
    {
        $this->id               = ( $prefix ? "{$prefix}-" : '' ) . bin2hex(random_bytes(10));

        return $this;
    }


    public function setJsonrpc(string $jsonrpc) : RpcRequest
    {
        $this->requestAssert($jsonrpc)->eq('2.0', 'The jsonrpc version must be 2.0');

        $this->jsonrpc          = $jsonrpc;

        return $this;
    }


    public function getJsonrpc() : string
    {
        $this->requestAssert($this->jsonrpc)->eq('2.0', 'The jsonrpc version must be 2.0');

        return $this->jsonrpc;
    }


    public function setMethod(string $method) : RpcRequest
    {
        $this->requestAssert($method)->notEmpty('The jsonrpc method must not be empty');

        $this->method           = $method;

        return $this;
    }


    public function getMethod() : string
    {
        $this->requestAssert($this->method)->notEmpty('The jsonrpc method must not be empty');

        return $this->method;
    }


    public function setParams($params) : RpcRequest
    {
        $this->params           = $params;

        return $this;
    }

    /**
     * @return stdClass
     */
    public function getParams()
    {
        return $this->params;
    }


    public function getParamStr(string $name) : string
    {
        $this->paramAssert($name)->scalar("The parameter '{$name}' is not a scalar value.");

        return (string)$this->params->{$name};
    }


    public function getParamNullStr(string $name) : string
    {
        $this->params->{$name}  = $this->params->{$name} ??0?: '';
        $this->paramAssert($name, false)->nullOr()->scalar("The parameter '{$name}' is not a scalar value.");

        return (string)$this->params->{$name};
    }


    public function getParamInt(string $name) : int
    {
        $this->paramAssert($name)->numeric("The parameter '{$name}' is not a numeric value.");

        return (int)$this->params->{$name};
    }


    public function getParamId(string $name) : int
    {
        $id = $this->getParamInt($name);
        $this->paramAssert($name)->value($id)->id("The parameter '{$name}' is not an ID.");

        return $id;
    }


    public function getParamFloat(string $name) : float
    {
        $this->paramAssert($name)->scalar("The parameter '{$name}' is not a scalar value.");

        return (float)$this->params->{$name};
    }


    public function getParamObj(string $name) : stdClass
    {
        $this->paramAssert($name)->isObject("The parameter '{$name}' is not an object.");

        return (object)$this->params->{$name};
    }


    public function getParamArr(string $name) : array
    {
        $this->paramAssert($name)->isArray("The parameter '{$name}' is not an object.");

        return (array)$this->params->{$name};
    }


    public function getParamBool(string $name) : bool
    {
        $this->paramAssert($name)->scalar("The parameter '{$name}' is not a scalar value.");

        return in_array($this->params->{$name}, ['1', 1, 'Yes', true, 'true']);
    }


    protected function paramAssert(string $name, bool $checkExists=true) : Assert
    {
        $test                   = Assert::that($this->params)
            ->fieldName($name)
            ->code(RpcError::ERROR_INVALID_PARAMS);
        if ( $checkExists )
        {
            $test->propertyExists($name, "The parameter '{$name}' was not specified.");
        }

        return Assert::that($this->params->{$name})
            ->fieldName($name)
            ->code(RpcError::ERROR_INVALID_PARAMS);
    }

    /**
     * @param mixed $value
     * @return Assert
     */
    protected function requestAssert($value) : Assert
    {
        return Assert::that($value)->code(RpcError::ERROR_INVALID_REQUEST);
    }

    /**
     * @return object
     */
    public function jsonSerialize()
    {
        return (object)$this->toArray();
    }


    public function toArray() : array
    {
        return [
            'jsonrpc'       => $this->getJsonrpc(),
            'method'        => $this->getMethod(),
            'params'        => $this->getParams(),
            'id'            => $this->getId(),
        ];
    }
}
