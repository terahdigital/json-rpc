<?php  declare(strict_types=1);

namespace Terah\JsonRpc;

use JsonSerializable;
use stdClass;

class RpcFieldError implements JsonSerializable
{
    protected string $name      = '';

    /** @var array|string[] */
    protected array $messages   = [];

    /**
     * RpcFieldError constructor.
     *
     * @param string $name
     * @param string[]  $messages
     */
    public function __construct(string $name, array $messages=[])
    {
        $this->setName($name);

        if ( ! empty($messages) )
        {
            $this->setMessages($messages);
        }
    }


    public function getName() : string
    {
        return $this->name;
    }


    public function setName(string $name) : RpcFieldError
    {
        $this->name             = $name;

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getMessages() : array
    {
        return $this->messages;
    }


    public function setMessages(array $messages) : RpcFieldError
    {
        $this->messages         = [];
        foreach ( $messages as $message )
        {
            is_array($message) ? $this->appendMessages($message) : $this->appendMessage($message);
        }

        return $this;
    }


    public function appendMessage(string $message) : RpcFieldError
    {
        $this->messages[]       = $message;

        return $this;
    }


    public function appendMessages(array $messages) : RpcFieldError
    {
        $this->messages = array_merge($this->messages, $messages);

        return $this;
    }

    public function jsonSerialize() : stdClass
    {
        return (object)$this->toArray();
    }


    public function toArray() : array
    {
        return [
            'name'          => $this->getName(),
            'messages'      => $this->getMessages()
        ];
    }
}
