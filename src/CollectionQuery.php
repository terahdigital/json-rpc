<?php declare(strict_types=1);


namespace Terah\JsonRpc;

use Terah\Assert\Assert;

use Terah\RestClient\RestResponseInterface;


/**
 * Class CollectionQuery
 *
 * @package Terah\JsonRpc
 *
 * @property string[] $fields   []
 * @property string[] $where    []
 * @property int $limit         100
 * @property int $offset        200
 * @property string $searchTerm s1234567
 * @property string $orderBy    username
 * @property string $orderDir   desc
 */
class CollectionQuery
{
    const STATUS_ACTIVE             = 1;
    const STATUS_INACTIVE           = 0;
    const STATUS_ARCHIVED           = -1;

    /** @var string[] $fields */
    protected array $fields         = [];

    protected array $where          = [];

    protected int $limit            = 0;

    protected int $offset           = 0;

    protected string $searchTerm    = '';

    protected string $orderBy       = '';

    protected string $orderDir      = '';

    protected ?RestResponseInterface $lastResponse = null;

    public function __construct(array $query=[])
    {
        if ( ! empty($query['_fields']) )
        {
            $this->fields   = explode('|', $query['_fields']);
        }
        unset($query['_fields']);
        if ( ! empty($query['_search']) )
        {
            $this->searchTerm   = (string)$query['_search'];
        }
        unset($query['_search']);
        if ( ! empty($query['_order']) )
        {
            $parts              = explode(',', $query['_order']);
            $this->orderDir     = empty($parts[1]) ? 'asc' : $parts[1];
            $this->orderBy      = $parts[0];
        }
        unset($query['_order']);
        if ( ! empty($query['_limit']) )
        {
            $this->limit    = (string)$query['_limit'];
        }
        unset($query['_limit']);
        if ( ! empty($query['_offset']) )
        {
            $this->offset   = (string)$query['_offset'];
        }
        unset($query['_offset']);
        foreach ( $query as $field => $value )
        {
            $this->where[$field]      = (string)$value;
        }
    }


    protected function select(array $fields=[]) : CollectionQuery
    {
        $this->fields = $fields;

        return $this;
    }


    public function where(array $where=[]) : CollectionQuery
    {
        $this->where            = array_merge($this->where, $where);

        return $this;
    }


    public function whereLike(string $field, string $value) : CollectionQuery
    {
        return $this->where([$field => "whereLike({$value})"]);
    }


    public function whereNotLike(string $field, string $value) : CollectionQuery
    {
        return $this->where([$field => "whereNotLike({$value})"]);
    }


    public function whereLt(string $field, string $value) : CollectionQuery
    {
        return $this->where([$field => "whereLt({$value})"]);
    }


    public function whereLte(string $field, string $value) : CollectionQuery
    {
        return $this->where([$field => "whereLte({$value})"]);
    }


    public function whereGt(string $field, string $value) : CollectionQuery
    {
        return $this->where([$field => "whereGt({$value})"]);
    }


    public function whereGte(string $field, string $value) : CollectionQuery
    {
        return $this->where([$field => "whereGte({$value})"]);
    }


    public function whereBetween(string $field, string $value1, string $value2) : CollectionQuery
    {
        return $this->where([$field => "whereBetween({$value1},{$value2})"]);
    }


    public function whereIn(string $field, array $values) : CollectionQuery
    {
        return $this->where([$field => implode('|', $values)]);
    }


    public function whereNotBetween(string $field, string $value1, string $value2) : CollectionQuery
    {
        return $this->where([$field => "whereNotBetween({$value1},{$value2})"]);
    }


    public function search(string $searchTerm) : CollectionQuery
    {
        $this->searchTerm       = $searchTerm;

        return $this;
    }


    public function whereActive() : CollectionQuery
    {
        return $this->whereStatus(self::STATUS_ACTIVE);
    }


    public function whereInactive() : CollectionQuery
    {
        return $this->whereStatus(self::STATUS_INACTIVE);
    }


    public function whereArchived() : CollectionQuery
    {
        return $this->whereStatus(self::STATUS_ARCHIVED);
    }


    public function whereStatus(int $status) : CollectionQuery
    {
        Assert::that($status)->status();

        return $this->where(['status' => $status]);
    }

    /**
     * ORDER BY $columnName (ASC | DESC)
     *
     * @param  string   $columnName - The name of the column or an expression
     * @param  string   $ordering   (DESC | ASC)
     * @return CollectionQuery
     */
    public function orderBy(string $columnName, string $ordering='asc') : CollectionQuery
    {
        Assert::that(strtolower($ordering))->inArray(['desc', 'asc']);

        $this->orderBy  = $columnName;
        $this->orderDir = strtolower($ordering);

        return $this;
    }


    public function limit(int $limit, int $offset=0) : CollectionQuery
    {
        $this->limit = $limit;
        if ( $offset )
        {
            $this->offset($offset);
        }

        return $this;
    }


    public function getLimit() : int
    {
        return $this->limit;
    }


    public function offset(int $offset) : CollectionQuery
    {
        $this->offset = $offset;

        return $this;
    }


    public function getOffset() : int
    {
        return $this->offset;
    }


    public function reset() : CollectionQuery
    {
        $this->fields           = [];
        $this->where            = [];
        $this->searchTerm       = null;
        $this->orderBy          = null;
        $this->orderDir         = null;
        $this->limit            = null;
        $this->offset           = null;

        return $this;
    }


    public function fetchQueryString() : string
    {
        $query                  = $this->fetchQuery();

        return http_build_query($query, '', ini_get('arg_separator.output'), PHP_QUERY_RFC3986);
    }


    public function fetchQuery() : array
    {
        $query = [];
        if ( ! empty($this->fields) )
        {
            $query['_fields']   = implode('|', $this->fields);
        }
        if ( ! empty($this->searchTerm) )
        {
            $query['_search']   = (string)$this->searchTerm;
        }
        foreach ( $this->where as $field => $value )
        {
            $query[$field]      = (string)$value;
        }
        if ( ! empty($this->orderBy) )
        {
            $direction          = empty($this->orderDir) ? 'asc' : $this->orderDir;
            $query['_order']    = "{$this->orderBy},{$direction}";
        }
        if ( ! empty($this->limit) )
        {
            $query['_limit']    = (string)$this->limit;
        }
        if ( ! empty($this->offset) )
        {
            $query['_offset']   = (string)$this->offset;
        }

        return $query;
    }


    public static function factory(array $params) : CollectionQuery
    {
        return new CollectionQuery($params);
    }
}
