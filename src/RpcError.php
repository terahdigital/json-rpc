<?php declare(strict_types=1);

namespace Terah\JsonRpc;

use JsonSerializable;
use stdClass;
use Terah\Assert\Assert;
use Terah\Assert\AssertionFailedException;
use Throwable;

class RpcError implements JsonSerializable
{
    protected int $code             = 0;

    protected string $message       = '';

    protected ?RpcFieldErrorCollection $data = null;

    const ERROR_INVALID_JSON        = -32700; //    Parse error	Invalid JSON was received by the server.  An error occurred on the server while parsing the JSON text.
    const ERROR_INVALID_REQUEST     = -32600; //	Invalid Request	The JSON sent is not a valid Request object.
    const ERROR_METHOD_NOT_FOUND    = -32601; //	Method not found	The method does not exist / is not available.
    const ERROR_INVALID_PARAMS      = -32602; //	Invalid params	Invalid method parameter(s).
    const ERROR_INTERNAL_RPC_ERROR  = -32603; //	Internal error	Internal JSON-RPC error.
    const ERROR_UNAUTHORISED        = -32000; //    Unauthorised
    const ERROR_INVALID_CREDENTIALS = -32001; //    Unauthorised
    //const ERROR_INVALID_JSON    = -32000 to -32099	Server error	Reserved for implementation-defined server-errors.

    static protected array $messages = [
        self::ERROR_INVALID_JSON        => 'Parse error.',      // Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.
        self::ERROR_INVALID_REQUEST     => 'Invalid Request.',  // The JSON sent is not a valid Request object.
        self::ERROR_METHOD_NOT_FOUND    => 'Method not found.', // The method does not exist / is not available.
        self::ERROR_INVALID_PARAMS      => 'Invalid params.',   // Invalid method parameter(s).
        self::ERROR_INTERNAL_RPC_ERROR  => 'Internal error.',   // Internal JSON-RPC error.
        self::ERROR_UNAUTHORISED        => 'Unauthorised.',     // Method not allowed
        self::ERROR_INVALID_CREDENTIALS => 'Invalid username or password.',     // Invalid username or password.
        // -32000 to -32099	Server error	Reserved for implementation-defined server-errors.
     ];

    static protected array $httpCodes = [
        self::ERROR_INVALID_JSON        => 400, // The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing)
        self::ERROR_INVALID_REQUEST     => 400, // The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing)
        self::ERROR_METHOD_NOT_FOUND    => 501, // The server either does not recognize the request method, or it lacks the ability to fulfil the request. Usually this implies future availability (e.g., a new feature of a web-service API).
        self::ERROR_INVALID_PARAMS      => 400, // The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing)
        self::ERROR_INTERNAL_RPC_ERROR  => 500, // A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
        self::ERROR_UNAUTHORISED        => 403, // The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource, or may need an account of some sort.
        self::ERROR_INVALID_CREDENTIALS => 401, // Specifically for use when authentication is required and has failed or has not yet been provided.
        // -32000 to -32099	Server error	// A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
    ];


    /**
     * RpcError constructor.
     * @param Throwable|null $e
     */
    public function __construct(Throwable $e=null)
    {
        $this->data             = new RpcFieldErrorCollection();
        if ( $e )
        {
            $code                   = (int)$e->getCode();
            $message                = $e->getMessage();
            $this->setCode($code ?: self::ERROR_INTERNAL_RPC_ERROR);
            $this->setMessage($message);
            if ( $e->getCode() === self::ERROR_INVALID_PARAMS )
            {
                $this->setMessage(static::$messages[self::ERROR_INVALID_PARAMS]);
            }
            if ( $this->getCode() === self::ERROR_INTERNAL_RPC_ERROR )
            {
                $this->setMessage(static::$messages[self::ERROR_INTERNAL_RPC_ERROR]);
            }
            if ( $e->getCode() === self::ERROR_INVALID_JSON )
            {
                $this->setMessage(static::$messages[self::ERROR_INVALID_JSON]);
            }
            if ( $e instanceof AssertionFailedException )
            {
                $this->setCode($code < 0 ? $code : self::ERROR_INVALID_PARAMS);
                $this->setMessage(static::$messages[self::ERROR_INVALID_PARAMS]);
                $this->setData((new RpcFieldErrorCollection)
                    ->appendFieldError(new RpcFieldError($e->getFieldName(), (array) $e->getMessage())));
            }
            if ( $e instanceof RpcException )
            {
                $this->setData($e->getData());
            }
        }
    }


    public function setMethodNotExistsError() : RpcError
    {
        return $this->setCode(self::ERROR_METHOD_NOT_FOUND, true);
    }


    public function getCode() : int
    {
        return $this->code;
    }


    public function setCode(int $code, bool $setDefaultMessage=false) : RpcError
    {
        $this->code = $code;
        if ( $setDefaultMessage && isset(static::$messages[$code]) )
        {
            $this->setMessage(static::$messages[$code]);
        }

        return $this;
    }


    public function getMessage() : string
    {
        return $this->message;
    }


    public function setMessage(string $message) : RpcError
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }


    public function setData(RpcFieldErrorCollection $data) : RpcError
    {
        $this->data = $data;

        return $this;
    }


    public function appendData(RpcFieldError $error) : RpcError
    {
        $this->data->appendFieldError($error);

        return $this;
    }


    public function jsonSerialize() : stdClass
    {
        $this->validate();

        return (object)[
            'code'          => $this->getCode(),
            'message'       => $this->getMessage(),
            'data'          => $this->getData(),
        ];
    }


    public function validate() : bool
    {
        Assert::that($this->code)
            ->int('The error code must be an integer')
            ->notEq(0, 'The error code must not be zero');

        Assert::that($this->message)
            ->notEmpty('The error must not be empty');

        return true;
    }


    public function getHttpStatusCode() : int
    {
        Assert::that($this->code)
            ->int('The error code must be an integer')
            ->notEq(0, 'The error code must not be zero');

        $code                   =  static::$httpCodes[$this->code] ?? 500;

        return $code ?? 500;
    }
}
