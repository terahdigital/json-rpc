<?php declare(strict_types=1);


namespace Terah\JsonRpc;


interface RpcServiceInterface
{

    public function handle(RpcRequest $request, RpcResponse $response) : RpcResponse;


    public function handleBatch(RpcBatchRequest $batch) : RpcBatchResponse;
}