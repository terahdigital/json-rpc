<?php declare(strict_types=1);

namespace Terah\JsonRpc;

use Terah\RestClient\RestResponseInterface;
use Terah\RestClient\RestResponse;
/**
 * Class RestResponse
 *
// * @package Prism\ApiClient
// * @property int count
// * @property int total
// * @property int limit
// * @property int offset
// * @property int page
// * @property int pages
// * @property array order
// * @property string self
 * @property string notification_title
 * @property string notification_body
 * @property array errors
// * @property string token
// * @property array filters
// * @property array fields
// * @property string method
// * @property mixed data
// * @property int status
// * @property string body
// * @property array headers
// * @property string curlError
// * @property int curlErrorNo
 */
class HttpResponse extends RestResponse
{
    protected array $_meta_data   = [
        'method'                => '',
        'status'                => 200,
        'body'                  => null,
        'headers'               => [],
        'curlError'             => '',
        'curlErrorNo'           => null,
        'notification_title'    => '',
        'notification_body'     => '',
        'errors'                => [],
    ];

    /**
     * @param string $name
     * @param mixed $value
     * @return RestResponseInterface
     */
    public function set(string $name, $value) : RestResponseInterface
    {
        if ( $name === 'headers' && is_array($value) )
        {
            return $this->parseHeaders($value);
        }
        // We don't want this throwing exceptions in invalid keys
        // as this maybe used inside the RestException class and
        // exceptions inside exceptions is a bit to hard to grok
        if ( ! array_key_exists($name, $this->_meta_data) )
        {
            return $this;
        }
        // Don't set it if it's not the same type
        if ( ! is_null($this->_meta_data[$name]) && gettype($this->_meta_data[$name]) !== gettype($value) )
        {
            return $this;
        }
        $this->_meta_data[$name] = $value;

        return $this;
    }

    /**
     * @param array $headers
     * @return $this|RestResponseInterface
     */
    protected function parseHeaders(array $headers) : RestResponseInterface
    {
        foreach ( $headers as $header => $value )
        {
            if ( ! preg_match('/^X-Prism-/', $header) )
            {
                continue;
            }
            $idx        = str_replace('-', '_', strtolower(preg_replace('/^X-Prism-/', '', $header)));
            $value      = json_decode($value);
            $value      = ! is_object($value) ? $value : json_decode(json_encode($value), true);
            $this->set($idx, $value);
            unset($headers[$header]);
        }
        $this->_meta_data['headers'] = $headers;

        return $this;
    }


    public function getErrorTitle() : string
    {
        return $this->_meta_data['notification_title'];
    }


    public function getErrorMessage() : string
    {
        return $this->_meta_data['notification_body'];
    }


    public function getNotification() : string
    {
        return implode(': ', [$this->getErrorTitle(), $this->getErrorMessage()]);
    }


    public function getValidationErrors() : array
    {
        return $this->_meta_data['errors'];
    }
}
