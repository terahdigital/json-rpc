<?php declare(strict_types=1);


namespace Terah\JsonRpc;


use Exception;

class RpcException extends Exception
{
    protected ?RpcFieldErrorCollection $data = null;

    public function getData() : RpcFieldErrorCollection
    {
        if ( ! $this->data )
        {
            return new RpcFieldErrorCollection();
        }

        return $this->data;
    }


    public function setData(RpcFieldErrorCollection $data) : RpcException
    {
        $this->data             = $data;

        return $this;
    }
}
