<?php declare(strict_types=1);

namespace Terah\JsonRpc;

use JsonSerializable;
use Terah\Assert\Assert;
use stdClass;

class RpcResponse implements JsonSerializable
{
    /** @var string  */
    protected string $jsonrpc   = '2.0';

    /** @var mixed  */
    protected $result           = null;

    protected ?RpcError $error  = null;

    /** @var mixed */
    protected $id               = null;


    public function __construct(?stdClass $data=null)
    {
        if ( ! $data )
        {
            return ;
        }
        Assert::that($data)
            ->code(-32700)
            ->isObject('Parse error: Invalid JSON was received by the server.');

        Assert::that($data)
            ->code(-32600)
            ->propertiesExist(['jsonrpc'], 'Invalid Request: The JSON sent is not a valid Request object.');

        $this
            ->setJsonrpc($data->jsonrpc)
            ->setId($data->id ?? null);

        if ( isset($data->error) )
        {
            $rpcError               = new RpcError;
            $rpcError->setCode($data->error->code ?? 0);
            $rpcError->setMessage($data->error->message ?? '');
            if ( ! empty($data->error->data) && is_array($data->error->data) )
            {
                foreach ( $data->error->data as $errors )
                {
                    if ( ! empty($errors->name) && ! empty($errors->messages) )
                    {
                        $rpcError->appendData(new RpcFieldError($errors->name, $errors->messages));
                    }
                }
            }
            $this->setError($rpcError);
        }
        if ( isset($data->result) )
        {
            $this->setResult($data->result);
        }
    }


    public function getJsonrpc() : string
    {
        Assert::that($this->jsonrpc)->eq('2.0', 'The jsonrpc version must be 2.0');

        return $this->jsonrpc;
    }


    public function setJsonrpc(string $jsonrpc) : RpcResponse
    {
        Assert::that($jsonrpc)->eq('2.0', 'The jsonrpc version must be 2.0');

        $this->jsonrpc          = $jsonrpc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     * @return RpcResponse
     */
    public function setResult($result) : RpcResponse
    {
        $this->result           = $result;

        return $this;
    }


    public function getError() : RpcError
    {
        return $this->error;
    }


    public function hasError() : bool
    {
        return ! is_null($this->error);
    }


    public function setError(RpcError $error) : RpcResponse
    {
        $this->error            = $error;

        return $this;
    }

    /**
     * @return string|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string|int $id
     * @return RpcResponse
     */
    public function setId($id) : RpcResponse
    {
        Assert::that($id)
            ->fieldName("id")
            ->code(RpcError::ERROR_INVALID_REQUEST);

        $this->id               = $id;

        return $this;
    }


    public function jsonSerialize() : stdClass
    {
        $result                 =  new stdClass();
        $result->jsonrpc        = $this->getJsonrpc();
        $result->result         = $this->getResult();
        if ( $this->error )
        {
            $result->error          = $this->getError();
        }
        if ( $this->id )
        {
            $result->id             = $this->getId();
        }

        return $result;
    }


    public function getHttpStatusCode() : int
    {
        if ( $this->error )
        {
            return $this->error->getHttpStatusCode();
        }

        return 200;
    }
}
